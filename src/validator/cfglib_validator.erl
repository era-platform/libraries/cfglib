%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 06.01.2021
%%% @doc validator facade. define validate strategy.

-module(cfglib_validator).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([validate_cfg/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec validate_cfg(Cfg::map(), OldCfg::map()) -> ok | {ok,Warnings::map()} | {error,Errors::map()}.
%% ---
validate_cfg(NewCfg,OldCfg) ->
    CState = #check_state{ncfg=NewCfg,ocfg=OldCfg},
    check_cfg_1(CState).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% structure
check_cfg_1(CState) ->
    CState1 = ?VlCfgCheckStruct:check(CState),
    check_cfg_2(CState1).

%% msvcs
check_cfg_2(CState) ->
    CState1 = ?VlCfgCheckMsvcs:check(CState),
    check_cfg_3(CState1).

%% nodes
check_cfg_3(CState) ->
    check_cfg_4(CState).

%% servers
check_cfg_4(CState) ->
    check_cfg_x(CState).

%% last
check_cfg_x(#check_state{errors=[],warnings=[]}) ->
    ResMap = #{},
    {ok,ResMap};
check_cfg_x(#check_state{errors=[],warnings=W}) ->
    ResMap = #{warnings => W},
    {ok,ResMap};
check_cfg_x(#check_state{errors=E,warnings=[]}) ->
    ResMap = #{errors => E},
    {error,ResMap};
check_cfg_x(#check_state{errors=E,warnings=W}) ->
    ResMap = #{errors => E,
               warnings => W},
    {error,ResMap}.