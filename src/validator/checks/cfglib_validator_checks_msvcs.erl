%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 05.05.2022 7:34
%%% @doc

-module(cfglib_validator_checks_msvcs).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([check/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec check(CState::#check_state{}) -> CState1::#check_state{}.
%% ---
check(CState) ->
    check_msvcs(CState).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
check_msvcs(CState) ->
    #check_state{ncfg=NCfg}=CState,
    NMsvcsData = ?CPB:get_msvcs_data(NCfg),
    check_msvc(NMsvcsData,CState).

%% ---
check_msvc([],CState) -> CState;
check_msvc([NMsvcData|T],CState) ->
    #check_state{ncfg=NCfg}=CState,
    MsvcType = maps:get(<<"type">>,NMsvcData),
    case ?DMsvc:is_exists_descriptor(MsvcType) of
        true ->
            case ?DMsvc:get_descriptor_info(MsvcType,validate_parameters_point) of
                undefined -> check_msvc(T,CState);
                [AppModule,AppFun,AppArgs] ->
                    CheckArgs = AppArgs++[NMsvcData,NCfg],
                    ArgsLen = erlang:length(CheckArgs),
                    case ?BU:function_exported(AppModule,AppFun,ArgsLen) of
                        false -> CState;
                        true ->
                            try erlang:apply(AppModule,AppFun,CheckArgs) of
                                ok -> check_msvc(T,CState);
                                Res ->
                                    CState1 = save_check_result(Res,NMsvcData,CState),
                                    check_msvc(T,CState1)
                            catch C:E:Stack ->
                                    ?LOG('$error',"'~s'. check_msvc. check crash: Microserivice:~120p~n~120p:~120p~nStack:~120p",[?MODULE,NMsvcData,C,E,Stack]),
                                    CState1 = save_check_result({crash,C,E},NMsvcData,CState),
                                    check_msvc(T,CState1)
                            end
                    end
            end;
        false ->
            ?LOG('$error',"'~s'. check_msvc. microservice descriptor not found. Microserivice:~120p",[?MODULE,NMsvcData]),
            CState1 = save_check_result(msvc_not_found,NMsvcData,CState),
            check_msvc(T,CState1)
    end.

%% ---
save_check_result(msvc_not_found,NMsvcData,CState) ->
    MsvcType = maps:get(<<"type">>,NMsvcData),
    MsvcName = maps:get(<<"name">>,NMsvcData),
    Err = {error, #{reason => <<"microservice descriptor not found">>,
                    microservice => #{type => MsvcType,
                                      name => MsvcName}
                   }},
    update_cstate(Err,CState);
save_check_result({crash,C,E},NMsvcData,CState) ->
    MsvcType = maps:get(<<"type">>,NMsvcData),
    MsvcName = maps:get(<<"name">>,NMsvcData),
    Err = {error, #{reason => <<"validate microservice crash">>,
                    crash_info => #{class => C,error => E},
                    microservice => #{type => MsvcType,
                                      name => MsvcName}
                   }},
    update_cstate(Err,CState);
save_check_result({ok,ResMap}=Ok,NMsvcData,CState) ->
    MsvcType = maps:get(<<"type">>,NMsvcData),
    MsvcName = maps:get(<<"name">>,NMsvcData),
    Ok = {ok, #{result => ok,
                info => ResMap,
                microservice => #{type => MsvcType,
                                  name => MsvcName}
               }},
    update_cstate(Ok,CState);
save_check_result({error,ErrInfo},NMsvcData,CState) ->
    MsvcType = maps:get(<<"type">>,NMsvcData),
    MsvcName = maps:get(<<"name">>,NMsvcData),
    Err = {error, #{reason => <<"validate microservice error">>,
                    info => ErrInfo,
                    microservice => #{type => MsvcType,
                                      name => MsvcName}
                   }},
    update_cstate(Err,CState).

%% ---
update_cstate({ok,ResMap},CState) ->
    #check_state{warnings=Warnings}=CState,
    CState#check_state{warnings=[ResMap|Warnings]};
update_cstate({error,ErrInfo},CState) ->
    #check_state{errors=Errors}=CState,
    CState#check_state{errors=[ErrInfo|Errors]}.