%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 01.11.2021 18:01
%%% @doc

-module(cfglib_validator_checks_structure).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([check/1,
         %
         test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec check(CState::#check_state{}) -> CState1::#check_state{}.
%% ---
check(CState) ->
    #check_state{ncfg=NCfg}=CState,
    Descriptor = get_cfg_descriptor(),
    Result = ?APPJSV:check(NCfg, Descriptor),
    parse_jsv_result(Result,CState).

%% debug
test() -> do_test().

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
parse_jsv_result({ok,ResMap},CState) when is_map_key(warnings,ResMap)==true ->
    #check_state{warnings=Warnings}=CState,
    W = maps:get(warnings,ResMap),
    CState#check_state{warnings=W++Warnings};
parse_jsv_result({ok,_},CState) ->
    CState;
parse_jsv_result({error,ResMap},CState) ->
    #check_state{errors=Errors,warnings=Warnings}=CState,
    E = maps:get(errors,ResMap),
    W = maps:get(warnings,ResMap,[]),
    CState#check_state{errors=E++Errors,warnings=W++Warnings}.


%% -----------
%% Cfg descriptor
%% -----------

%% ---
get_cfg_descriptor() ->
    #{<<"type">> => object,
      <<"object_rule">> =>
          #{<<"req">> =>
                #{<<"microservices">> => get_cfg_descriptor_microservices(),
                  <<"servers">> => get_cfg_descriptor_servers()},
            <<"opt">> =>
                #{
                  % used by cfglib parts
                  <<"nodes">> => get_cfg_descriptor_nodes(),
                  <<"structure">> => get_cfg_descriptor_structure(),
                  % custom parts (may be read from cfg_descriptor.json)
                  <<"aliases">> => get_cfg_descriptor_aliases(),
                  <<"general">> => get_cfg_descriptor_general(),
                  <<"domains">> => get_cfg_descriptor_domains()}
           }
     }.

%% ---
%% used by cfglib
%% ---

%% @private
get_cfg_descriptor_microservices() ->
    #{<<"type">> => array,
      <<"array_element">> =>
          #{<<"type">> => object,
            <<"skip_unknown_keys">> => true,
            <<"object_rule">> =>
                #{<<"req">> =>
                      #{<<"name">> => #{<<"type">> => str},
                        <<"type">> => #{<<"type">> => str}}
                 }
           }
     }.

%% @private
get_cfg_descriptor_nodes() ->
    #{<<"type">> => array,
      <<"array_element">> =>
          #{<<"type">> => object,
            <<"skip_unknown_keys">> => true,
            <<"object_rule">> =>
                #{<<"req">> =>
                      #{<<"name">> => #{<<"type">> => str}},
                  <<"opt">> =>
                      #{<<"iface">> => #{<<"type">> => str}}
                 }
           }
     }.

%% @private
get_cfg_descriptor_servers() ->
    #{<<"type">> => array,
      <<"array_element">> =>
          #{<<"type">> => object,
            <<"skip_unknown_keys">> => true,
            <<"object_rule">> =>
                #{<<"req">> =>
                      #{<<"name">> => #{<<"type">> => str},
                        <<"ifaces">> => #{<<"type">> => array,
                                          <<"array_element">> =>
                                              #{<<"type">> => object,
                                                <<"object_rule">> =>
                                                    #{<<"req">> =>
                                                          #{<<"key">> => #{<<"type">> => str},
                                                            <<"value">> => #{<<"type">> => str}}
                                                     }
                                               }
                                         },
                        <<"logiface">> => #{<<"type">> => str}}
                 }
           }
     }.

%% @private
get_cfg_descriptor_structure() ->
    #{<<"type">> => array,
      <<"array_element">> =>
          #{<<"type">> => object,
            <<"object_rule">> =>
                #{<<"req">> =>
                      #{<<"server">> => #{<<"type">> => str}},
                  <<"opt">> =>
                      #{<<"microservices">> => #{<<"type">> => strlist},
                        <<"nodes">> =>
                            #{<<"type">> => array,
                              <<"array_element">> =>
                                  #{<<"type">> => object,
                                    <<"object_rule">> =>
                                        #{<<"req">> =>
                                              #{<<"name">> => #{<<"type">> => str},
                                                <<"microservices">> => #{<<"type">> => strlist}}
                                         }
                                   }
                             }
                       }
                 }
           }
     }.

%% ---
%% custom parts
%% ---

%% @private
get_cfg_descriptor_aliases() ->
    #{<<"type">> => object,
      <<"object_rule">> =>
          #{<<"opt">> =>
                #{<<"paths">> => #{<<"type">> => array, <<"array_element">> => #{<<"type">> => object}},
                  <<"pgdb_strings">> => #{<<"type">> => array, <<"array_element">> => #{<<"type">> => object}}
                 }
           }
     }.

%% @private
get_cfg_descriptor_domains() ->
    #{<<"type">> => array,
      <<"array_element">> => #{<<"type">> => object}}.

%% @private
get_cfg_descriptor_general() ->
    #{<<"type">> => object}.

%% -----------
%% debug
%% -----------

do_test() ->
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/cfglib/ebin"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/basiclib/ebin"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/jsv/ebin"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/jsx/ebin"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/loggerlib/ebin"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/iconverl/ebin"),
    application:ensure_all_started(jsv),
    %
    Files = ["/home/ceceron/develop/era/ci/scripts/simple_build/cfg.json",
             "/home/ceceron/develop/era/ci/scripts/simple_build/cfg2srv_localtest.json",
             "/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test.json",
             "/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test3srv.json",
             "/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test_4check.json",
             "/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test_4validate_test_full_keys.json"],
    do_check_file(Files).

%% @private
do_check_file([]) -> ok;
do_check_file([FilePath|T]) ->
    ?OUTL("~p. do_check_file. Start check file: ~120p",[?MODULE, FilePath]),
    {ok,F} = file:read_file(FilePath),
    ?OUTL("1"),
    Cfg = jsx:decode(F,[return_maps]),
    ?OUTL("2"),
    CState = #check_state{ncfg=Cfg},
    ?OUTL("3"),
    #check_state{errors=[]}=check(CState),
    ?OUTL("4"),
    do_check_file(T).
