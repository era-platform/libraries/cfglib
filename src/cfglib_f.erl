%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 09.01.2021 17:33
%%% @doc

-module(cfglib_f).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%% common
-export([get_cfg_hash/0]).

%% ?CPS
-export([get_general_domain/0,
         get_general_param/1,
         %
         get_domain_info/1,
         %
         get_servers_by_param/2,
         get_server_info/1,
         %
         get_msvcs_by_param/2,
         get_msvcs_by_params/1,
         %
         get_node_info/1,
         %
         get_current_server_info/0,
         get_current_server_node/0,
         get_current_node_info/0,
         get_current_msvcs_info/0]).

%% ?CPB
-export([get_general_data/0,
         get_aliases_data/0,
         get_nodes_data/0, get_nodes_data/1,
         get_servers_data/0, get_servers_data/1,
         get_msvcs_data/0,
         get_domains_data/0,
         %
         get_alias_value/1,
         parse_alias/1,
         domains_aliases_to_fqdn/1,
         get_srv_host_by_iface/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------
%% common functions
%% ------------------

%% ---
-spec get_cfg_hash() -> Phash2::integer().
%% ---
get_cfg_hash() ->
    erlang:phash2(get_current_cfg()).

%% ------------------
%% ?CPS
%% ------------------

%% ---
-spec get_domain_info(DomainFqdn::binary()) -> {ok,DomainInfo::map()} | {error,not_found}.
%% ---
get_domain_info(DomainFqdn) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),DomainFqdn).

%% ---
-spec get_servers_by_param(ParamPath::[binary()],ParamValue::term()) -> [ServerInfo::map()].
%% ---
get_servers_by_param(ParamPath,ParamValue) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),ParamPath,ParamValue).

%% ---
-spec get_server_info(SrvName::binary()) -> {ok,SrvInfo::map()} | {error,not_found}.
%% ---
get_server_info(SrvName) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),SrvName).

%% ---
-spec get_msvcs_by_params(SearchInfo::[{ParamPath::[binary()],ParamValue::term()}]) -> [MsvcInfo::map()].
%% ---
get_msvcs_by_params(SearchInfo) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),SearchInfo).

%% ---
-spec get_msvcs_by_param(ParamPath::[binary()],ParamValue::term()) -> [MsvcInfo::map()].
%% ---
get_msvcs_by_param(ParamPath,ParamValue) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),ParamPath,ParamValue).

%% ---
-spec get_node_info(Node::atom()) -> {ok,NodeInfo::map()} | {error,not_found}.
%% ---
get_node_info(Node) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),Node).

%% ---
-spec get_general_domain() -> DomainName::binary().
%% ---
get_general_domain() ->
    ?CPS:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_general_param(Key::binary()) -> {ok,Value::term()} | {error,not_found}.
%% ---
get_general_param(Key) ->
    ?CPS:?FUNCTION_NAME(get_current_cfg(),Key).

%% ---
-spec get_current_server_info() -> ServerInfo::map() | no_return().
%% ---
get_current_server_info() ->
    ?CPS:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_current_server_node() -> SrvNode::atom() | no_return.
%% ---
get_current_server_node() ->
    ?CPB:?FUNCTION_NAME().

%% ---
-spec get_current_node_info() -> NodeInfo::map().
%% ---
get_current_node_info() ->
    ?CPS:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_current_msvcs_info() -> [MsvcInfo::map()].
%% ---
get_current_msvcs_info() ->
    ?CPS:?FUNCTION_NAME(get_current_cfg()).

%% ------------------
%% ?CPB
%% ------------------

%% ---
-spec get_general_data() -> Data::map().
%% ---
get_general_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_aliases_data() -> Data::map().
%% ---
get_aliases_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_nodes_data(Filters::list()) -> Data::[map()].
%% ---
get_nodes_data(Filters) ->
    ?CPB:?FUNCTION_NAME(get_current_cfg(),Filters).

%% ---
-spec get_nodes_data() -> Data::[map()].
%% ---
get_nodes_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_servers_data(Filters::list()) -> [Data::map()].
%% ---
get_servers_data(Filters) ->
    ?CPB:?FUNCTION_NAME(get_current_cfg(),Filters).

%% ---
-spec get_servers_data() -> [Data::map()].
%% ---
get_servers_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_msvcs_data() -> [Data::map()].
%% ---
get_msvcs_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_domains_data() -> [Data::map()].
%% ---
get_domains_data() ->
    ?CPB:?FUNCTION_NAME(get_current_cfg()).

%% ---
-spec get_alias_value(Alias::binary()) -> {ok,Value::binary()} | {error,Reason::binary()}.
%% ---
get_alias_value(Alias) ->
    ?CPB:?FUNCTION_NAME(get_current_cfg(),Alias).

%% ---
-spec parse_alias(Alias::binary()) -> {ok,AliasPart::binary(),AliasName::binary()} | {error,Reason::binary()}.
%% ---
parse_alias(Alias) when is_binary(Alias) ->
    ?CPB:?FUNCTION_NAME(Alias).

%% ---
-spec domains_aliases_to_fqdn([DomainAliases::binary()]) -> {[Fqdn::binary()],[UnresolvedAlias::binary()]}.
%% ---
domains_aliases_to_fqdn(DomainAliases) ->
    ?CPB:?FUNCTION_NAME(get_current_cfg(),DomainAliases).

%% ---
-spec get_srv_host_by_iface(Iface::binary(),SrvName::binary()) -> {ok,Host::binary()} | {error,not_found}.
%% ---
get_srv_host_by_iface(Iface,SrvName) ->
    ?CPB:?FUNCTION_NAME(get_current_cfg(),Iface,SrvName).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
get_current_cfg() -> ?CU:get_curr_cfg().