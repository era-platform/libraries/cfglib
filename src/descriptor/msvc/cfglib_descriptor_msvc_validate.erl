%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 22.05.2022 15:44
%%% @doc

-module(cfglib_descriptor_msvc_validate).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([validate_app_descriptor/1,
         %
         check_app_point/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec validate_app_descriptor(AppDescriptor::map()) -> {ok,Info::map()} | {error,ErrInfo::map()}.
%% ---
validate_app_descriptor(AppDescriptor) ->
    Descriptor = get_app_jsv_descriptor(),
    ?APPJSV:check(AppDescriptor, Descriptor).

%% ---
-spec check_app_point(Key::binary(),Value::term(),MapArgs::map()) -> ok | {error,Info::map()}.
%% ---
check_app_point(Key,[AppModule,AppF,AppArgs],MapArgs) ->
    case erlang:is_binary(AppModule) andalso erlang:is_binary(AppF) andalso erlang:is_list(AppArgs) of
        true -> ok;
        false ->
            AppDescriptor = maps:get(json,MapArgs),
            Err = #{error => <<"invalid app_point value">>,
                    key => Key,
                    info => <<"module and function must be string, parameters list">>,
                    descriptor => AppDescriptor},
            {error,Err}
    end;
check_app_point(Key,_,MapArgs) ->
    AppDescriptor = maps:get(json,MapArgs),
    Err = #{error => <<"invalid app_point format">>,
            key => Key,
            descriptor => AppDescriptor},
    {error, Err}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
get_app_jsv_descriptor() ->
    #{<<"type">> => object,
      <<"object_rule">> =>
          #{<<"req">> =>
                #{<<"microservices">> => get_app_jsv_descriptor_msvcs()}
           }
     }.

%% ---
get_app_jsv_descriptor_msvcs() ->
    #{<<"type">> => array,
      <<"array_element">> =>
          #{<<"type">> => object,
            <<"skip_unknown_keys">> => true,
            <<"object_rule">> =>
                #{<<"req">> =>
                      #{<<"name">> => #{<<"type">> => str},
                        <<"start_point">> => #{<<"type">> => array,
                                               <<"checkfun">> => [?MODULE, check_app_point, #{}]},
                        <<"params">> => #{<<"type">> => object}
                       },
                  <<"opt">> =>
                      #{<<"os_cmd_params">> => #{<<"type">> => str},
                        <<"erl_cmd_params">> => #{<<"type">> => str},
                        <<"validate_parameters_point">> => #{<<"type">> => array,
                                                             <<"checkfun">> => [?MODULE, check_app_point, #{}]},
                        <<"update_parameters_point">> => #{<<"type">> => array,
                                                           <<"checkfun">> => [?MODULE, check_app_point, #{}]},
                        <<"update_configuration_point">> => #{<<"type">> => array,
                                                              <<"checkfun">> => [?MODULE, check_app_point, #{}]}
                       }
                 }
           }
     }.
