%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 05.05.2021 18:23
%%% @doc

-module(cfglib_descriptor_msvc).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([load_app_descriptors/0,
         get_descriptor_info/2,
         is_exists_descriptor/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec load_app_descriptors() -> ok.
%% ---
load_app_descriptors() ->
    CodeRootPath = ?BU:to_unicode_list(?U:drop_last_subdir(code:which(?MODULE), 4)),
    FilesRaw = filelib:wildcard(CodeRootPath++"/*/*/priv/descriptor.json"),
    Files = filter_descriptors_files(FilesRaw),
    load_app_descriptor_files(Files),
    ok.

%% ---
-spec get_descriptor_info(Msvc::binary(),Key::atom()) -> Result::term() | no_return.
%% ---
get_descriptor_info(Msvc,Key) ->
    [#msvc_descriptor{}=RD] = ets:lookup(?DescrEts,Msvc),
    get_msvc_descr_info(Key,RD).

%% ---
-spec is_exists_descriptor(Msvc::binary()) -> boolean().
%% ---
is_exists_descriptor(Msvc) ->
    case ets:lookup(?DescrEts,Msvc) of
        [] -> false;
        [#msvc_descriptor{}] -> true
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------
%% load descriptors
%% ------------------

%% ---
filter_descriptors_files(FilesRaw) ->
    FFilter = fun(FilePath) ->
                      Splitted = lists:reverse(filename:split(FilePath)),
                      [_,_,ServiceFolder,ServiceName|_] = Splitted,
                      ServiceFolder==ServiceName
              end,
    lists:filter(FFilter,FilesRaw).

%% ---
load_app_descriptor_files([]) -> ok;
load_app_descriptor_files([FilePath|T]) ->
    case read_app_descriptor_file(FilePath) of
        {ok,Data} ->
            case ?DMvscVl:validate_app_descriptor(Data) of
                {ok,_} ->
                    DescrRecs = parse_app_descriptor_data(Data,FilePath),
                    ets:insert(?DescrEts,DescrRecs);
                {error,ErrInfo} ->
                    ?LOG('$error',"'~s'. validate_app_descriptor_data. validate file '~s'. error: ~120p.",[?MODULE,FilePath,ErrInfo]),
                    ok
            end;
        {error,_} -> ok
    end,
    load_app_descriptor_files(T).

%% @private
read_app_descriptor_file(FilePath) ->
    case file:read_file(FilePath) of
        {error,_}=Err -> Err;
        {ok,Data} ->
            case catch jsx:decode(Data,[return_maps]) of
                {'EXIT',_} ->
                    ?LOG('$error',"'~s'. read_app_descriptor_file. decode file '~s' error.",[?MODULE,FilePath]),
                    {error, <<"invalid json">>};
                Descriptor when is_map(Descriptor) -> {ok,Descriptor};
                _ ->
                    ?LOG('$error',"'~s'. read_app_descriptor_file. invalid file '~s' content. object is expected.",[?MODULE,FilePath]),
                    {error,<<"invalid format. object is expected">>}
            end
    end.

%% @private
parse_app_descriptor_data(Descriptor,FilePath) ->
    [Msvcs] = ?BU:maps_get([<<"microservices">>],Descriptor),
    [parse_app_descriptor_msvcs(Msvc,FilePath) || Msvc <- Msvcs].

%% ---
parse_app_descriptor_msvcs(MsvcData,FilePath) ->
    [Name,SP,Params] = ?BU:maps_get([<<"name">>,<<"start_point">>,<<"params">>],MsvcData),
    [OsCmdParams,ErlCmdParams,VPP,UPP,UCP] = ?BU:maps_get_default([{<<"os_cmd_params">>,<<>>},
                                                                   {<<"erl_cmd_params">>,<<>>},
                                                                   {<<"validate_parameters_point">>,undefined},
                                                                   {<<"update_parameters_point">>,undefined},
                                                                   {<<"update_configuration_point">>,undefined}],MsvcData),
    #msvc_descriptor{name=Name,
                     start_point=SP,
                     validate_params_point=VPP,
                     update_params_point=UPP,
                     update_cfg_point=UCP,
                     params=Params,
                     app_path=?BU:to_binary(?U:drop_last_subdir(FilePath,2)),
                     os_cmd_params=OsCmdParams,
                     erl_cmd_params=ErlCmdParams}.

%% ------------------
%% get info from descriptors
%% ------------------

%% ---
get_msvc_descr_info(name,#msvc_descriptor{name=Name}) -> Name;
get_msvc_descr_info(os_cmd_params,#msvc_descriptor{os_cmd_params=OCP}) -> OCP;
get_msvc_descr_info(erl_cmd_params,#msvc_descriptor{erl_cmd_params=ECP}) -> ECP;
get_msvc_descr_info(start_point,#msvc_descriptor{start_point=SP}) -> SP;
get_msvc_descr_info(validate_parameters_point,#msvc_descriptor{validate_params_point=UPP}) -> UPP;
get_msvc_descr_info(update_params_point,#msvc_descriptor{update_params_point=UPP}) -> UPP;
get_msvc_descr_info(update_cfg_point,#msvc_descriptor{update_cfg_point=UCP}) -> UCP;
get_msvc_descr_info(app_path,#msvc_descriptor{app_path=AppPath}) -> AppPath.