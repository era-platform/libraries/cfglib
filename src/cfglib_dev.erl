%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 09.05.2021 14:48
%%% @doc

-module(cfglib_dev).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([load_cfg/1,
         validate/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec validate(CfgPath::binary()) -> {ok,ResMap::map()} | {error,ErrMap::map()}.
%% ---
validate(CfgPath) when is_binary(CfgPath) ->
    ?OUTL("dev. validate cfgptah:~120p",[CfgPath]),
    case read_cfg(CfgPath) of
        {ok,CfgMap} ->
            OldCfg = ?APP:get_prev_cfg(),
            ?APP:validate_cfg(CfgMap,OldCfg);
        {error,_}=Err -> Err
    end.

%% ---
-spec load_cfg(CfgPath::binary()) -> {ok,loaded} | {error,Reason::term()}.
%% ---
load_cfg(CfgPath) ->
    ?OUTL("dev. load_cfg cfgptah:~120p",[CfgPath]),
    case read_cfg(CfgPath) of
        {ok,CfgMap} ->
            case catch ?CU:create_curr_cfg_beam(CfgMap,CfgMap) of
                {'EXIT',ErrDescr0} -> {error,{create_cfg_beam_crash, ErrDescr0}};
                ok -> {ok,loaded};
                {error,ErrDescr1} -> {error, {create_cfg_beam_error, ErrDescr1}}
            end;
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
-spec read_cfg(CfgPath::binary()) -> {ok,Cfg::map()} | {error,ErrMap::term()}.
%% ---
read_cfg(CfgPath) ->
    case file:read_file(CfgPath) of
        {ok,CfgRaw} ->
            case catch jsx:decode(CfgRaw,[return_maps]) of
                {'EXIT',_} -> {error,<<"cfg is invalid json">>};
                CfgMap -> {ok,CfgMap}
            end;
        {error,Err} ->
            {error,{read_file_error,Err}}
    end.