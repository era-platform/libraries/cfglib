%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 05.05.2021
%%% @doc

-module(cfglib_supv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(StartArgs) ->
    ?OUT('$trace',"Supv. inited with args:~120p", [StartArgs]),
    init_ets(),
    SupFlags = #{strategy => one_for_one,
                 intensity => 10,
                 period => 2},
    {ok, {SupFlags, []}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
init_ets() ->
    CreateEtsRes = ets:new(?DescrEts, [named_table,{keypos, #msvc_descriptor.name},public]),
    ?OUT('$trace',"Supv. create descriptors ets result:~120p",[CreateEtsRes]),
    ok.