%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 08.01.2021 16:39
%%% @doc

-module(cfglib_cfg_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([create_curr_cfg_beam/2,
         get_curr_cfg/0,
         get_curr_cfg_raw/0,
         get_prev_cfg/0,
         get_prev_cfg_raw/0
        ]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

-define(CCfg, cfglib_cfg_curr).
-define(PCfg, cfglib_cfg_prev).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec create_curr_cfg_beam(CfgRaw::map(),Cfg::map()) -> ok | {error,Reason::term()}.
%% ---
%% create beam with: 1. raw cfg - without defaults and syscfg and 2. cfg after apply defaults and syscfg
create_curr_cfg_beam(CfgRaw,Cfg) ->
    case catch get_curr_cfg() of
        {error,_} ->
            CurrRes = create_cfg_beam(?CCfg,CfgRaw,Cfg),
            _R = handle_create_res(CurrRes,?CCfg),
            CurrRes;
        OldCfg when is_map(OldCfg) ->
            OldCfgRaw = get_curr_cfg_raw(),
            case create_cfg_beam(?CCfg,CfgRaw,Cfg) of % create current cfg beam
                ok ->
                    case create_cfg_beam(?PCfg,OldCfgRaw,OldCfg) of % create priv cfg beam
                        ok ->
                            handle_create_res(ok,?CCfg),
                            handle_create_res(ok,?PCfg),
                            ok;
                        {error,_}=ErrPrev -> ErrPrev
                    end;
                {error,_}=ErrCurr -> ErrCurr
            end
    end.

%% -----------
%% return cfg functions
%% -----------

%% ---
-spec get_curr_cfg() -> Cfg::map() | no_return().
%% ---
get_curr_cfg() ->
    Module = ?CCfg,
    case catch erlang:apply(Module,get_cfg,[]) of
        {'EXIT',_} -> throw({error,not_loaded});
        Cfg when is_map(Cfg) -> Cfg
    end.

%% ---
-spec get_curr_cfg_raw() -> Cfg::map() | no_return().
%% ---
get_curr_cfg_raw() ->
    Module = ?CCfg,
    case catch erlang:apply(Module,get_cfg_raw,[]) of
        {'EXIT',_} -> throw({error,not_loaded});
        Cfg when is_map(Cfg) -> Cfg
    end.

%% ---
-spec get_prev_cfg() -> Cfg::map() | undefined.
%% ---
get_prev_cfg() ->
    Module = ?PCfg,
    case catch erlang:apply(Module,get_cfg,[]) of
        {'EXIT',_} -> undefined;
        Cfg when is_map(Cfg) -> Cfg
    end.

%% ---
-spec get_prev_cfg_raw() -> Cfg::map() | undefined.
%% ---
get_prev_cfg_raw() ->
    Module = ?PCfg,
    case catch erlang:apply(Module,get_cfg_raw,[]) of
        {'EXIT',_} -> undefined;
        Cfg when is_map(Cfg) -> Cfg
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
-spec create_cfg_beam(ModuleName:: ?CCfg | ?PCfg, CfgRaw::map(), Cfg::map()) -> ok | {error,Reason::term()}.
%% ---
create_cfg_beam(ModuleName,CfgRaw,Cfg)
  when (ModuleName==?CCfg orelse ModuleName==?PCfg) andalso is_map(CfgRaw) andalso is_map(Cfg) ->
    ModForm = mod_form(ModuleName),
    ExportForm = export_form(),
    FunctionsForm = func_forms(CfgRaw,Cfg),
    {ok, ModuleName, FileBin} = compile:forms([ModForm,ExportForm]++FunctionsForm),
    %
    FilePath = filename:join([filename:dirname(code:which(?MODULE)),?BU:to_list(ModuleName)++".beam"]),
    ?OUTL("Create cfg beam. Write to filepath:~120p",[FilePath]),
    file:write_file(FilePath, FileBin).

%% @private
mod_form(ModuleName) ->
    Module = erl_syntax:attribute(erl_syntax:atom(module),[erl_syntax:atom(ModuleName)]),
    _ModForm =  erl_syntax:revert(Module).

%% @private
export_form() ->
    ExportFuns = [erl_syntax:arity_qualifier(erl_syntax:atom(get_cfg),erl_syntax:integer(0)), % cfg with syscfg and defaults
                  erl_syntax:arity_qualifier(erl_syntax:atom(get_cfg_raw),erl_syntax:integer(0))], % cfg without it
    Export = erl_syntax:attribute(erl_syntax:atom(export),[erl_syntax:list(ExportFuns)]),
    _ExportForm = erl_syntax:revert(Export).

%% @private
func_forms(CfgRaw,Cfg) ->
    Funs = [fun_get_cfg(Cfg),
            fun_get_cfg_raw(CfgRaw)],
    lists:map(fun(FunX) -> erl_syntax:revert(FunX) end, Funs).

%% @private
%% return cfg as is
fun_get_cfg(Cfg) ->
    Body = erl_syntax:abstract(Cfg),
    Clause =  erl_syntax:clause([],[],[Body]),
    _Fun =  erl_syntax:function(erl_syntax:atom(get_cfg),[Clause]).

%% @private
%% return cfg as is
fun_get_cfg_raw(CfgRaw) ->
    Body = erl_syntax:abstract(CfgRaw),
    Clause =  erl_syntax:clause([],[],[Body]),
    _Fun =  erl_syntax:function(erl_syntax:atom(get_cfg_raw),[Clause]).

%% ---
handle_create_res(ok,ModuleName) -> c:l(ModuleName);
handle_create_res({error,_},_ModuleName) -> ok.