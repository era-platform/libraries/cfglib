%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 15.06.2021 18:57
%%% @doc

-module(cfglib_cfg_defaults).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([apply_defaults/1]).

-export([test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

-define(MLogPrefix, "DefaultCfg").

%% ====================================================================
%% Public functions
%% ====================================================================

test() ->
    {ok,CfgRaw}=file:read_file("/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test.json"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/jsx/ebin/"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/basiclib/ebin/"),
    Cfg = jsx:decode(CfgRaw,[return_maps]),
    apply_defaults(Cfg).

%% ---
-spec apply_defaults(Cfg::map()) -> NewCfg::map().
%% ---
apply_defaults(Cfg) ->
    Cfg1 = apply_nodes_defaults(Cfg),
    apply_structure_defaults(Cfg1).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------
%% Nodes defaults
%% ------------------

%% ---
apply_nodes_defaults(Cfg) ->
    case maps:get(<<"nodes">>,Cfg,undef) of
        undef ->
            ?LOG('$trace',"~p. apply_nodes_defaults. part 'nodes' is missed, start apply defaults",[?MLogPrefix]),
            do_apply_nodes_defaults(Cfg);
        _ ->
            ?LOG('$trace',"~p. apply_nodes_defaults. part 'nodes' is exists, apply defaults skipped",[?MLogPrefix]),
            Cfg
    end.

%% @private
do_apply_nodes_defaults(Cfg) ->
    Msvcs = maps:get(<<"microservices">>,Cfg,[]),
    FDefNodes =
        fun(MsvcItem,Acc) ->
                case maps:get(<<"name">>,MsvcItem,undef) of
                    undef ->
                        ?LOG('$trace',"~p. do_apply_nodes_defaults. microservice missed 'name', generate default node skipped~nMsvc:~120p",[?MLogPrefix,MsvcItem]),
                        Acc;
                    MsvcName ->
                        NodeItem = #{<<"name">> => MsvcName},
                        [NodeItem|Acc]
                end
        end,
    NodesCfgPart = lists:foldl(FDefNodes,[],Msvcs),
    Cfg#{<<"nodes">> => NodesCfgPart}.

%% ------------------
%% Structure defaults
%% ------------------

%% ---
apply_structure_defaults(Cfg) ->
    IsExistsStruct = maps:is_key(<<"structure">>,Cfg),
    ServersCount =
        case maps:get(<<"servers">>,Cfg,undef) of
            V when is_list(V) -> erlang:length(V);
            _ -> -1
        end,
    case IsExistsStruct==false andalso ServersCount==1 of
        true ->
            ?LOG('$trace',"~p. apply_structure_defaults. part 'structure' is missed and the only 1 server is present, start apply defaults",[?MLogPrefix]),
            do_apply_structure_defaults(Cfg);
        false when IsExistsStruct==true ->
            ?LOG('$trace',"~p. apply_structure_defaults. part 'structure' is exists but more than 1 server are present, start apply 'microservice' key defaults",[?MLogPrefix]),
            do_apply_structure_msvc_defaults(Cfg);
        false ->
            ?LOG('$trace',"~p. apply_structure_defaults. part 'structure' is exists or servers count is not 1, apply defaults skipped",[?MLogPrefix]),
            Cfg
    end.

%% @private
do_apply_structure_defaults(Cfg) ->
    [ServerItem] = maps:get(<<"servers">>,Cfg),
    ServerName = maps:get(<<"name">>,ServerItem,undef),
    Nodes = maps:get(<<"nodes">>,Cfg),
    %
    FDefServerNodes = fun(NodeItem,Acc) ->
                              NodeName = maps:get(<<"name">>,NodeItem),
                              MsvcName = NodeName, % this is so, in the default case
                              StructNodeItem = #{<<"name">> => NodeName,
                                                 <<"microservices">> => [MsvcName]},
                              [StructNodeItem|Acc]
                      end,
    ServerNodes = lists:foldl(FDefServerNodes,[],Nodes),
    StructItem = #{<<"server">> => ServerName,
                   <<"nodes">> => ServerNodes},
    Cfg#{<<"structure">> => [StructItem]}.

%% @private
do_apply_structure_msvc_defaults(Cfg) ->
    Structure = maps:get(<<"structure">>,Cfg),
    Structure1 =
        case do_apply_structure_msvc_defaults_1(Structure,[]) of
            error -> Structure;
            ok -> Structure;
            {ok, NewStruct} -> NewStruct
        end,
    Cfg#{<<"structure">> => Structure1}.

%% @private
do_apply_structure_msvc_defaults_1([],Res) -> {ok,Res};
do_apply_structure_msvc_defaults_1([StructItem|T],Acc) when is_map(StructItem) ->
    IsExistsNodes = maps:is_key(<<"nodes">>,StructItem),
    Msvcs = maps:get(<<"microservices">>,StructItem,undef),
    case Msvcs/=undef andalso IsExistsNodes==false of
        false -> ok;
        true when is_list(Msvcs) ->
            IsListOfBin = lists:all(fun(V) -> erlang:is_binary(V) end, Msvcs),
            case IsListOfBin of
                true ->
                    StructItem1 = maps:remove(<<"microservices">>,StructItem),
                    NewNodes = [#{<<"name">> => MsvcName, <<"microservices">> => [MsvcName]} || MsvcName <- Msvcs],
                    StructItem2 = StructItem1#{<<"nodes">> => NewNodes},
                    do_apply_structure_msvc_defaults_1(T,[StructItem2|Acc]);
                false ->
                    ?LOG('$trace',"~p. do_apply_structure_msvc_defaults_1. 'microservices' must be list of strings! apply defaults skipped",[?MLogPrefix]),
                    error
            end;
        true ->
            ?LOG('$trace',"~p. do_apply_structure_msvc_defaults_1. 'microservices' must be list(of strings). apply defaults skipped",[?MLogPrefix]),
            error
    end;
do_apply_structure_msvc_defaults_1(_,_Acc) ->
    ?LOG('$trace',"~p. do_apply_structure_msvc_defaults_1. 'structure' must be list(of objects). apply defaults skipped",[?MLogPrefix]),
    error.
