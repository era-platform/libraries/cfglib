%%% coding: utf-8
%%% ------
%%%
%%% Copyright (c) 2020 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% ------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 14.03.2019 19:42
%%% @doc Additional configuration parser.
%%%      Does not work directly with the configuration, uses the results of the base module.
%%%      Provides the most frequently used features for system services.

-module(cfglib_cfg_parser_special).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_general_domain/1,
         get_general_param/2,
         %
         get_domain_info/2,
         %
         get_servers_by_param/3,
         get_server_info/2,
         %
         get_msvcs_by_param/3,
         get_msvcs_by_params/2,
         get_msvcs_info/2,
         %
         get_node_info/2,
         %
         get_current_server_info/1,
         get_current_node_info/1,
         get_current_msvcs_info/1,
         %
         bp_get_item/2,
         bp_get_parent_item/2,
         bp_get_n_parent_item/3]).

-export([test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

test() ->
    {ok,Cfg} = cfglib_cfg_syscfg:test(),
    io:format("Cfg:~120p~n~n~n",[Cfg]),
    get_current_msvcs_info(Cfg).

%% ------------------
%% DOMAINS functions
%% ------------------

%% ---
-spec get_domain_info(Cfg::map(),DomainFqdn::binary()) -> {ok,DomainInfo::map()} | {error,not_found}.
%% ---
get_domain_info(Cfg,DomainFqdn) ->
    case ?CPB:get_domains_data(Cfg,[{fldeq,[{<<"fqdn">>,DomainFqdn}]}]) of
        [DomainInfo] -> {ok,DomainInfo};
        [] -> {error,not_found}
    end.

%% ------------------
%% SERVER functions
%% ------------------

%% ---
-spec get_servers_by_param(Cfg::map(),ParamPath::[binary()],ParamValue::term()) -> [ServerInfo::map()].
%% ---
get_servers_by_param(Cfg,ParamPath,ParamValue) ->
    [Server || Server <- ?CPB:get_servers_data(Cfg), ?BU:parse_map(ParamPath,Server)==ParamValue].

%% ---
-spec get_server_info(Cfg::map(),SrvName::binary()) -> {ok,SrvInfo::map()} | {error,not_found}.
%% ---
get_server_info(Cfg,SrvName) ->
    case ?CPB:get_servers_data(Cfg,[{fldeq,[{<<"name">>,SrvName}]}]) of
        [SrvInfo] -> {ok,SrvInfo};
        [] -> {error,not_found}
    end.

%% ------------------
%% Microservices functions
%% ------------------

%% ---
-spec get_msvcs_by_params(Cfg::map(),SearchInfo::[{ParamPath::[binary()],ParamValue::term()}]) -> [MsvcInfo::map()].
%% ---
get_msvcs_by_params(Cfg,SearchInfo) ->
    MsvcData = ?CPB:get_msvcs_data(Cfg),
    F = fun(MsvcInfo) ->
                FAny = fun({ParamPath,ParamValue}) -> ?BU:parse_map(ParamPath,MsvcInfo)==ParamValue end,
                lists:any(FAny,SearchInfo)
        end,
    lists:filter(F,MsvcData).

%% ---
-spec get_msvcs_by_param(Cfg::map(),ParamPath::[binary()],ParamValue::term()) -> [MsvcInfo::map()].
%% ---
get_msvcs_by_param(Cfg,ParamPath,ParamValue) ->
    [Msvc || Msvc <- ?CPB:get_msvcs_data(Cfg), ?BU:parse_map(ParamPath,Msvc)==ParamValue].

%% ---
-spec get_msvcs_info(Cfg::map(),Node::atom()) -> {ok,[MsvcsInfo::map()]} | {error,not_found}.
%% ---
get_msvcs_info(Cfg,Node) ->
    case ?CPB:get_msvcs_data(Cfg,[{fldintersect,[{<<"nodes">>,[Node]}]}]) of
        [] -> {error,not_found};
        [_|_]=MsvcsInfo -> {ok,MsvcsInfo}
    end.

%% ------------------
%% Nodes functions
%% ------------------

%% ---
-spec get_node_info(Cfg::map(),Node::atom()) -> {ok,NodeInfo::map()} | {error,not_found}.
%% ---
get_node_info(Cfg,Node) ->
    case ?CPB:get_nodes_data(Cfg,[{fldeq,[{<<"node">>,Node}]}]) of
        [NodeInfo] -> {ok,NodeInfo};
        [] -> {error,not_found}
    end.

%% ------------------
%% GENERAL functions
%% ------------------

%% ---
-spec get_general_domain(Cfg::map()) -> DomainName::binary().
%% ---
get_general_domain(Cfg) ->
    maps:get(<<"generaldomain">>,?CPB:get_general_data(Cfg)).

%% ---
-spec get_general_param(Cfg::map(),Key::binary()) -> {ok,Value::term()} | {error,not_found}.
%% ---
get_general_param(Cfg,Key) ->
    case maps:get(Key,?CPB:get_general_data(Cfg),undef) of
        undef -> {error,not_found};
        V -> {ok,V}
    end.

%% ------------------
%% Own server & node & msvc functions
%% ------------------

%% ---
-spec get_current_server_info(Cfg::map()) -> ServerInfo::map() | no_return().
%% ---
get_current_server_info(Cfg) ->
    SrvNode = ?BU:get_env(?APP,?EnvSrvNode, undef),
    {ok,ServerInfo} = get_server_info(Cfg,?BU:node_name(SrvNode)),
    ServerInfo.

%% ---
-spec get_current_node_info(Cfg::map()) -> NodeInfo::map().
%% ---
get_current_node_info(Cfg) ->
    Node = erlang:node(),
    {ok,NodeInfo} = get_node_info(Cfg,Node),
    NodeInfo.

%% ---
-spec get_current_msvcs_info(Cfg::map()) -> [MsvcInfo::map()].
%% ---
get_current_msvcs_info(Cfg) ->
    Node = erlang:node(),
    {ok,MsvcsInfo} = get_msvcs_info(Cfg,Node),
    MsvcsInfo.

%% ------------------
%% SPECIALYZED functions
%% ------------------

%% ---
-spec bp_get_item(Path::binary(),Cfg::map()) -> Result::term().
%% ---
bp_get_item(Path,Cfg) ->
    bp_get_n_parent_item(0,Path,Cfg).

%% ---
-spec bp_get_parent_item(Path::binary(),Cfg::map()) -> Result::term().
%% ---
bp_get_parent_item(Path,Cfg) ->
    bp_get_n_parent_item(1,Path,Cfg).

%% ---
-spec bp_get_n_parent_item(N::integer(),Path::binary(),Cfg::map()) -> Result::term().
%% ---
bp_get_n_parent_item(N,Path,Cfg) ->
    ParsePath = bp_parse_path(Path),
    SplPath = lists:sublist(ParsePath,erlang:length(ParsePath)-N),
    bp_parse_cfg(SplPath,Cfg).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
bp_parse_path(Path) when is_binary(Path) ->
    F = fun(V) ->
                case ?BU:is_int_val(V) of
                    true -> ?BU:to_int(V)+1;
                    false -> V
                end
        end,
    lists:map(F,binary:split(Path,<<"/">>,[global,trim_all])).

%% ---
bp_parse_cfg([],Cfg) -> Cfg;
bp_parse_cfg([Key|T],Cfg) when is_binary(Key) ->
    bp_parse_cfg(T,maps:get(Key,Cfg));
bp_parse_cfg([Key|T],Cfg) when is_integer(Key) ->
    bp_parse_cfg(T,lists:nth(Key,Cfg)).