%%% coding: utf-8
%%% --------------------------------------
%%%
%%% Copyright (c) 2020 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% --------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 09.01.2021
%%% @doc Configuration parsing.
%%%      Knows about the structure of the configuration, and provides the converted data to external services.
%%%      Only this module can work directly with the configuration

-module(cfglib_cfg_parser_base).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([get_general_data/1,
         get_aliases_data/1,
         get_msvcs_data/1, get_msvcs_data/2,
         get_nodes_data/1, get_nodes_data/2,
         get_servers_data/1, get_servers_data/2,
         get_current_server_node/0,
         get_domains_data/1, get_domains_data/2]).

-export([get_alias_value/2,
         parse_alias/1,
         domains_aliases_to_fqdn/2,
         get_srv_host_by_iface/3,
         get_srv_node_iface/2]).

-export([test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

test() ->
    {ok,Cfg} = cfglib_cfg_syscfg:test(),
    get_nodes_data(Cfg),
    io:format("Cfg:~120p~n~n~n",[Cfg]),
    get_msvcs_data(Cfg).

%% ---
%% return params in general category
%% ---
-spec get_general_data(Cfg::map()) -> Data::map().
%% ---
get_general_data(Cfg) ->
    GeneralData = maps:get(<<"general">>,Cfg),
    GenDomainAlias = maps:get(<<"generaldomain">>,GeneralData),
    {[GenFqdn],_} = domains_aliases_to_fqdn(Cfg,[GenDomainAlias]),
    GeneralData#{<<"generaldomain">> => GenFqdn,
                 <<"generaldomain_alias">> => GenDomainAlias}.

%% ---
%% return params in aliases category
%% ---
-spec get_aliases_data(Cfg::map()) -> Data::map().
%% ---
get_aliases_data(Cfg) ->
    maps:get(<<"aliases">>,Cfg).

%% ---
%% return complete nodes data with filters
%% ---
-spec get_msvcs_data(Cfg::map(),Filters::list()) -> [Data::map()].
%% ---
get_msvcs_data(Cfg,Filters) ->
    MsvcsData = get_msvcs_data(Cfg),
    apply_struct_part_filters(Filters,MsvcsData).

%% ---
%% return params in microservices category
%% ---
-spec get_msvcs_data(Cfg::map()) -> [Data::map()].
%% ---
get_msvcs_data(Cfg) ->
    StructMsvcs = get_msvcs_from_structure(Cfg),
    build_msvcs_data(Cfg,StructMsvcs).

%% @private
get_msvcs_from_structure(Cfg) ->
    Structure = get_structure_data(Cfg),
    Servers = maps:get(<<"servers">>,Cfg),
    Nodes = maps:get(<<"nodes">>,Cfg),
    F0 = fun(StructServerItem,Acc) ->
                 [StructSrvName,StructNodes] = ?BU:maps_get([<<"server">>,<<"nodes">>],StructServerItem),
                 %
                 [SrvItem] = [SrvI || SrvI <- Servers, maps:get(<<"name">>,SrvI)==StructSrvName],
                 ServerName = maps:get(<<"name">>,SrvItem),
                 %
                 NodeNames = [maps:get(<<"name">>,NodeI) || NodeI <- StructNodes],
                 NodeItems = [NodeI || NodeI <- Nodes, lists:member(maps:get(<<"name">>,NodeI),NodeNames)],
                 %
                 NodesCache = fill_nodes_cache(NodeItems,ServerName,Cfg,Acc),
                 %
                 F2 = fun(StructNodeItem,AccY) ->
                              StructNodeName = maps:get(<<"name">>,StructNodeItem),
                              StructMsvcsNames = maps:get(<<"microservices">>,StructNodeItem),
                              %
                              Node = maps:get(StructNodeName,NodesCache),
                              %
                              F3 = fun(MsvcName,AccZ) ->
                                           FUpd = fun(MsvcData) -> fill_struct_msvc_data(MsvcData,StructSrvName,Node) end,
                                           maps:update_with(MsvcName,FUpd,fill_struct_msvc_data(#{},StructSrvName,Node),AccZ)
                                   end,
                              lists:foldl(F3, AccY, StructMsvcsNames)
                      end,
                 lists:foldl(F2, Acc, StructNodes)
         end,
    lists:foldl(F0,#{},Structure).

%% @private
fill_nodes_cache([],_,_,Acc) -> Acc;
fill_nodes_cache([NodeItem|T],ServerName,Cfg,Acc) ->
    NodeName = maps:get(<<"name">>,NodeItem),
    {_,NodeHost} = get_node_iface(NodeItem,ServerName,Cfg),
    Acc1 = Acc#{NodeName => ?BU:to_atom_new(<<NodeName/binary,"@",NodeHost/binary>>)},
    fill_nodes_cache(T,ServerName,Cfg,Acc1).

%% @private
fill_struct_msvc_data(MsvcData,SrvName,NodeName) when map_size(MsvcData)==0 ->
    #{<<"servers">> => [SrvName],
      <<"nodes">> => [NodeName]};
fill_struct_msvc_data(MsvcData,SrvName,NodeName) ->
    Srvs = maps:get(<<"servers">>,MsvcData),
    Nodes = maps:get(<<"nodes">>,MsvcData),
    MsvcData#{<<"servers">> => [SrvName|Srvs],
              <<"nodes">> => [NodeName|Nodes]}.

%% @private
build_msvcs_data(Cfg,StructMsvcs) ->
    Msvcs = maps:get(<<"microservices">>,Cfg),
    MsvcsData = fill_msvcs_params(Msvcs,StructMsvcs,0,Cfg),
    maps:values(MsvcsData).

%% @private
fill_msvcs_params([],StructMsvcs,_,_) -> StructMsvcs;
fill_msvcs_params([MsvcItem|T],StructMsvcs,MsvcOrder,Cfg) ->
    MsvcName = maps:get(<<"name">>,MsvcItem),
    MsvcType = maps:get(<<"type">>,MsvcItem),
    %
    case maps:is_key(MsvcName,StructMsvcs) of
        true ->
            FUpdate = fun(MsvcInfo) ->
                              MsvcServers = maps:get(<<"servers">>,MsvcInfo),
                              MsvcInfo#{<<"name">> => MsvcName,
                                        <<"type">> => MsvcType,
                                        <<"servers">> => lists:usort(MsvcServers),
                                        <<"params">> => MsvcItem}
                      end,
            StructMsvcs1 = maps:update_with(MsvcName,FUpdate,StructMsvcs),
            fill_msvcs_params(T,StructMsvcs1,MsvcOrder+1,Cfg);
        false -> fill_msvcs_params(T,StructMsvcs,MsvcOrder+1,Cfg) %% @todo удалить case если по валидации не может быть микросервис не указанный ни в одной ноде, иначе кейс валиден
    end.

%% ---
%% return complete nodes data with filters
%% ---
-spec get_nodes_data(Cfg::map(),Filters::list()) -> [Data::map()].
%% ---
get_nodes_data(Cfg,Filters) ->
    NodesData = get_nodes_data(Cfg),
    apply_struct_part_filters(Filters,NodesData).

%% ---
%% return complete nodes data
%% ---
-spec get_nodes_data(Cfg::map()) -> [Data::map()].
%% ---
get_nodes_data(Cfg) ->
    StructNodes = get_nodes_from_structure(Cfg),
    build_nodes_data(Cfg,StructNodes).

%% @private
get_nodes_from_structure(Cfg) ->
    Structure = get_structure_data(Cfg),
    F0 = fun(StructServerItem,Acc) ->
                 [SrvName,Nodes] = ?BU:maps_get([<<"server">>,<<"nodes">>],StructServerItem),
                 F2 = fun(StructNodeItem,{AccY,StructOrder}) ->
                              NodeName = maps:get(<<"name">>,StructNodeItem),
                              MsvcsNames = maps:get(<<"microservices">>,StructNodeItem),
                              AccY1 = AccY#{NodeName =>
                                                #{<<"server">> => SrvName,
                                                  <<"name">> => NodeName,
                                                  <<"microservices">> => MsvcsNames,
                                                  <<"structure_order">> => StructOrder}},
                              {AccY1,StructOrder+1}
                      end,
                 {AccX,_} = lists:foldl(F2, {Acc,0}, Nodes),
                 AccX
         end,
    lists:foldl(F0,#{},Structure).

%% @private
build_nodes_data(Cfg,StructNodes) ->
    Nodes = maps:get(<<"nodes">>,Cfg),
    NodesData = fill_nodes_params(Nodes,StructNodes,0,Cfg),
    maps:values(NodesData).

%% @private
fill_nodes_params([],NodesData,_,_) -> NodesData;
fill_nodes_params([NodeItem|T],NodesData,NodeOrder,Cfg) ->
    NodeName = maps:get(<<"name">>,NodeItem),
    %
    FUpdate = fun(NodeInfo) ->
                      ServerName = maps:get(<<"server">>,NodeInfo),
                      NodesMsvcsNames = maps:get(<<"microservices">>,NodeInfo),
                      %
                      {NodeIface,NodeHost} = get_node_iface(NodeItem,ServerName,Cfg),
                      Node = ?BU:to_atom_new(<<NodeName/binary,"@",NodeHost/binary>>),
                      %
                      MsvcsData = get_node_msvcs_data(NodesMsvcsNames,Cfg),
                      %
                      NodeInfo#{
                                <<"node">> => Node,
                                <<"iface">> => NodeIface,
                                <<"host">> => NodeHost,
                                <<"params">> => NodeItem,
                                <<"microservices">> => MsvcsData,
                                <<"node_order">> => NodeOrder
                               }
              end,
    NodesData1 = maps:update_with(NodeName,FUpdate,NodesData),
    fill_nodes_params(T,NodesData1,NodeOrder+1,Cfg).

%% @private
get_node_msvcs_data(NodesMsvcsNames,Cfg) ->
    Msvcs = maps:get(<<"microservices">>,Cfg),
    FFilter = fun(MsvcItem,{Acc,MsvcOrder}) ->
                      MsvcName = maps:get(<<"name">>,MsvcItem),
                      Acc1 =
                          case lists:member(MsvcName,NodesMsvcsNames) of
                              true ->
                                  FToAtom = fun(K,V,AccX) -> AccX#{?BU:to_atom_new(K) => V} end,
                                  MsvcItem1 = maps:fold(FToAtom,#{},MsvcItem),
                                  [MsvcItem1|Acc];
                              false -> Acc
                          end,
                      {Acc1,MsvcOrder+1}
              end,
    {MsvcsData,_} = lists:foldl(FFilter,{[],0},Msvcs),
    MsvcsData.

%% @private
get_node_iface(NodeItem,ServerName,Cfg) ->
    case maps:get(<<"iface">>,NodeItem,undef) of
        undef ->
            {ok,NodeIface,NodeHost} = get_srv_node_iface(Cfg,ServerName),
            {NodeIface,NodeHost};
        NodeIface ->
            {ok, Host} = get_srv_host_by_iface(Cfg,NodeIface,ServerName),
            {NodeIface,Host}
    end.

%% ---
%% return servers category with filters
%% ---
-spec get_servers_data(Cfg::map(),Filters::list()) -> [Data::map()].
%% ---
get_servers_data(Cfg,Filters) ->
    ServersData = get_servers_data(Cfg),
    apply_struct_part_filters(Filters,ServersData).

%% ---
-spec get_current_server_node() -> SrvNode::atom() | no_return.
%% ---
get_current_server_node() ->
    case ?BU:get_env(?APP,?EnvSrvNode, undef) of
        undef -> exit(<<"environment not set">>);
        V -> V
    end.

%% ---
%% return servers category
%% ---
-spec get_servers_data(Cfg::map()) -> [Data::map()].
%% ---
get_servers_data(Cfg) ->
    F = fun(SrvData,{Acc,SrvOrder}) ->
                [LI,SrvName] = ?BU:maps_get([<<"logiface">>,<<"name">>],SrvData),
                {ok, SrvHost} = get_srv_host_by_iface(Cfg,LI,SrvName),
                SrvNode = ?BU:to_atom_new(<<SrvName/binary,"@",SrvHost/binary>>),
                %
                Acc1 = #{<<"params">> => SrvData,
                         <<"logiface">> => SrvHost,
                         <<"logiface_alias">> => LI,
                         <<"node">> => SrvNode,
                         <<"server_order">> => SrvOrder,
                         <<"name">> => SrvName},
                {[Acc1|Acc],SrvOrder+1}
        end,
    {ServersData,_} = lists:foldl(F,{[],0},maps:get(<<"servers">>,Cfg)),
    ServersData.

%% ---
%% return domains category with filters
%% ---
-spec get_domains_data(Cfg::map(),Filters::list()) -> [Data::map()].
%% ---
get_domains_data(Cfg,Filters) ->
    DomainsData = get_domains_data(Cfg),
    apply_struct_part_filters(Filters,DomainsData).

%% ---
%% return domains category
%% ---
-spec get_domains_data(Cfg::map()) -> [Data::map()].
%% ---
get_domains_data(Cfg) ->
    Domains = maps:get(<<"domains">>,Cfg),
    F = fun(DomainData,{Acc,DO}) ->
                Acc1 = [#{<<"params">> => DomainData,
                          <<"domain_order">> => DO}|Acc],
                {Acc1,DO+1}
        end,
    {Info,_} = lists:foldl(F,{[],0},Domains),
    Info.

%% ====================================================================
%% Support functions
%% ====================================================================

%% ---
%% for the future translator
%% ---
-spec get_alias_value(Cfg::map(),Alias::binary()) -> {ok,Value::binary()} | {error,Reason::binary()}.
%% ---
get_alias_value(Cfg,Alias) ->
    case parse_alias(Alias) of
        {ok,AliasPart,AliasName} ->
            case ?BU:parse_map([<<"aliases">>,AliasPart],Cfg) of
                not_found -> {error,not_found};
                VList ->
                    Res = [maps:get(<<"value">>,A,undef) || A <- VList, maps:get(<<"alias">>,A,undef)==AliasName],
                    case Res of
                        [V] -> {ok,V};
                        _ -> {error,unknown_result}
                    end
            end;
        {error,_}=Err -> Err
    end.

%% ---
-spec parse_alias(Alias::binary()) -> {ok,AliasPart::binary(),AliasName::binary()} | {error,Reason::atom()}.
%% ---
parse_alias(Alias) when is_binary(Alias) ->
    try
        <<"alias://",APath/binary>> = Alias,
        [AliasPart|AliasNameL] = binary:split(APath,<<"/">>,[global]),
        {ok,AliasPart,?BU:join_binary(AliasNameL,<<"/">>)}
    catch
        _:_ -> {error,invalid_format}
    end.

%% ---
-spec domains_aliases_to_fqdn(Cfg::map(),[DomainAliases::binary()]) -> {[Fqdn::binary()],[UnresolvedAlias::binary()]}.
%% ---
domains_aliases_to_fqdn(_,[]) -> {[],[]};
domains_aliases_to_fqdn(Cfg,DomainAliases) ->
    DomainsData = get_domains_data(Cfg),
    AliasDomains = [{maps:get(<<"name">>,DD),maps:get(<<"fqdn">>,DD)} || DD <- DomainsData],
    F = fun(DAlias,{Fqdns,Unresolveds}) ->
                case ?BU:get_by_key(DAlias,AliasDomains,undef) of
                    undef -> {Fqdns,[DAlias|Unresolveds]};
                    V -> {[V|Fqdns],Unresolveds}
                end
        end,
    lists:foldl(F,{[],[]},DomainAliases).

%% ---
-spec get_srv_host_by_iface(Cfg::map(),Iface::binary(),SrvName::binary()) -> {ok,Host::binary()} | {error,not_found}.
%% ---
get_srv_host_by_iface(Cfg,Iface,SrvName) ->
    Servers = maps:get(<<"servers">>,Cfg),
    F = fun(_,{ok,_}=Acc) -> Acc;
           (SrvItem,Acc) ->
                case maps:get(<<"name">>,SrvItem)==SrvName of
                    true ->
                        case maps:get(<<"ifaces">>,SrvItem,undefined) of
                            undefined -> Acc;
                            Ifaces when is_map(Ifaces) -> {ok,maps:get(Iface,Ifaces,{error,not_found})};
                            Ifaces when is_list(Ifaces) ->
                                case [maps:get(<<"value">>,Ifc) || Ifc <- Ifaces, maps:get(<<"key">>,Ifc)==Iface] of
                                    [IfaceValue|_] -> {ok,IfaceValue};
                                    _ -> Acc
                                end
                        end;
                    false -> Acc
                end
        end,
    lists:foldl(F,{error,not_found},Servers).

%% ---
-spec get_srv_node_iface(Cfg::map(),SrvName::binary()) -> {ok,NodeIface::binary(),NodeHost::binary()} | {error,not_found}.
%% ---
get_srv_node_iface(Cfg,SrvName) ->
    Servers = maps:get(<<"servers">>,Cfg),
    F = fun(_,{ok,_}=Acc) -> Acc;
           (SrvItem,Acc) ->
                case maps:get(<<"name">>,SrvItem)==SrvName of
                    true ->
                        [Ifaces,NodeIface] = ?BU:maps_get([<<"ifaces">>,<<"node_iface">>],SrvItem),
                        case [maps:get(<<"value">>,Ifc) || Ifc <- Ifaces, maps:get(<<"key">>,Ifc)==NodeIface] of
                            [IfaceValue|_] -> {ok,NodeIface,IfaceValue};
                            _ -> Acc
                        end;
                    false -> Acc
                end
        end,
    lists:foldl(F,{error,not_found},Servers).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
get_structure_data(Cfg) ->
    maps:get(<<"structure">>,Cfg).

%% ---
apply_struct_part_filters([],XPartData) -> XPartData;
apply_struct_part_filters(Filters,XPartData) ->
    case lists:keytake(select,1,Filters) of
        false -> do_apply_struct_part_filters(Filters,XPartData);
        {value,{select,Selects},Filters1} ->
            FilteredXData = do_apply_struct_part_filters(Filters1,XPartData),
            do_apply_struct_part_selects(Selects,FilteredXData)
    end.

%% @private
do_apply_struct_part_filters([],FilteredXPartData) -> FilteredXPartData;
do_apply_struct_part_filters([{FilterKey,FilterValues}|T],XPartData) ->
    XData1 = apply_one_struct_filter(FilterKey,FilterValues,XPartData),
    do_apply_struct_part_filters(T,XData1).

%% @private
apply_one_struct_filter(_,[],XPartData) -> XPartData;
%%
apply_one_struct_filter(FKey,[{K,V}|T],XPartData) when FKey==fldeq; FKey==fldin; FKey==fldexists; FKey==fldintersect ->
    Def = erlang:make_ref(),
    F =
        case binary:split(?BU:to_binary(K),<<".">>,[global]) of
            [<<"params">>,MsvcParam|T] ->
                Keys = [<<"params">>,?BU:to_atom_new(MsvcParam)|T],
                get_filter_fun(FKey,Keys,V,Def);
            [_|_]=Keys -> get_filter_fun(FKey,Keys,V,Def);
            _ -> fun(_) -> false end
        end,
    XD1 = lists:filter(F,XPartData),
    apply_one_struct_filter(FKey,T,XD1).

%% @private
get_filter_fun(fldeq,Keys,Value,Default) -> fun(XData) -> ?BU:parse_map(Keys,XData,Default)==Value end;
get_filter_fun(fldin,Keys,Values,Default) when is_list(Values) ->
    fun(XData) -> lists:member(?BU:parse_map(Keys,XData,Default),Values) end;
get_filter_fun(fldintersect,Keys,Values,Default) when is_list(Values) ->
    fun(XData) ->
            ItemValue = ?BU:parse_map(Keys,XData,Default),
            case is_list(ItemValue) of
                true -> ?BU:intersection(ItemValue,Values)/=[];
                false -> false
            end
    end;
get_filter_fun(fldexists,Keys,IsNeedExists,Default) when is_boolean(IsNeedExists) ->
    fun(XData) ->
            case ?BU:parse_map(Keys,XData,Default) of
                Default when IsNeedExists -> false;
                Default -> true;
                _ when IsNeedExists -> true;
                _ -> false
            end
    end.

%% @private
do_apply_struct_part_selects(Fields,XPartData) when is_list(Fields) ->
    F = fun(XData) -> maps:with(Fields,XData) end,
    lists:map(F,XPartData);
do_apply_struct_part_selects({K,Fields},XPartData) when is_list(Fields) ->
    case binary:split(K,<<".">>,[global]) of
        [_|_]=Keys ->
            F = fun(XData) ->
                        PXData = ?BU:parse_map(Keys,XData),
                        maps:with(Fields,PXData)
                end,
            lists:map(F,XPartData);
        _ -> []
    end.
