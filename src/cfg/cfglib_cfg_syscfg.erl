%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 13.06.2021 15:18
%%% @doc

-module(cfglib_cfg_syscfg).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([apply_syscfg/2]).

-export([test/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

-define(MLogPrefix, "SysCfg").

%% ====================================================================
%% Public functions
%% ====================================================================

test() ->
    %  реализовать дефолты аналогично дефолтам над конфигом, если тут структуры нет, то генерим ноды по микросервисам и генерим структуру по нодам
    {ok,SysCRaw}=file:read_file("/home/ceceron/develop/era/ci/scripts/simple_build/syscfg.json"),
    %{ok,CfgRaw}=file:read_file("/home/ceceron/develop/era/ci/scripts/simple_build/cfg.json"),
    {ok,CfgRaw}=file:read_file("/home/ceceron/develop/era/ci/scripts/simple_build/cfg_test3srv.json"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/jsx/ebin/"),
    true = code:add_patha("/home/ceceron/develop/era/libraries/cfglib/_build/default/lib/basiclib/ebin/"),
    ?BU:set_env(cfglib,server_node,'srv_test@ceceron.ru'),
    SysC = jsx:decode(SysCRaw,[return_maps]),
    Cfg = jsx:decode(CfgRaw,[return_maps]),
    Cfg1 = cfglib_cfg_defaults:apply_defaults(Cfg),
    apply_syscfg(Cfg1,SysC).

%% ---
-spec apply_syscfg(Cfg::map(),SysCfg::map()) -> {ok,NewCfg::map()}.
%% ---
apply_syscfg(Cfg,SysCfgRaw) ->
    SysCfg = apply_syscfg_defaults(SysCfgRaw),
    CfgX1 = merge_nodes_in_cfg(Cfg,SysCfg),
    CfgX2 = merge_microservices_in_cfg(CfgX1, SysCfg),
    CfgX3 = merge_microservices_evw_in_cfg(CfgX2,SysCfg),
    {ok,CfgX3}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------
%% Add nodes from syscfg
%% ------------------

apply_syscfg_defaults(SysCfg) ->
    % реализована генерация structure в системном конфиге, если этот раздел отсутствует, то добавляем в него все ноды указанные в разделе nodes
    % если он есть, то ничего не делаем в отличии от дефолта для конфига, на основании microservices генерить нельзя т.к. они добавляются в каждую ноду
    IsExistsStructure = maps:is_key(<<"structure">>,SysCfg),
    IsExistsNodes = maps:is_key(<<"nodes">>,SysCfg),
    case IsExistsStructure==false andalso IsExistsNodes==true of
        true ->
            ?LOG('$trace',"~p. apply_syscfg_defaults. part 'structure' is missed and exists part 'nodes', start apply defaults",[?MLogPrefix]),
            do_apply_sysstructure_defaults(SysCfg);
        false ->
            ?LOG('$trace',"~p. apply_syscfg_defaults. part 'structure' is exists or missed part 'nodes', apply defaults skipped",[?MLogPrefix]),
            SysCfg
    end.

%% @private
do_apply_sysstructure_defaults(SysCfg) ->
    Nodes = maps:get(<<"nodes">>,SysCfg),
    %
    FDefNodes = fun(NodeItem,Acc) ->
                        NodeName = maps:get(<<"name">>,NodeItem),
                        StructNodeItem = #{<<"name">> => NodeName,
                                           <<"microservices">> => []}, % in default case microservices are empty
                        [StructNodeItem|Acc]
                end,
    ServerNodes = lists:foldl(FDefNodes,[],Nodes),
    StructItem = #{<<"nodes">> => ServerNodes},
    SysCfg#{<<"structure">> => StructItem}.

%% ------------------
%% Add nodes from syscfg
%% ------------------

merge_nodes_in_cfg(Cfg,SysCfg) ->
    SysNodes = get_syscfg_part(<<"nodes">>,list,SysCfg),
    ?LOG('$trace',"~p. merge_nodes_in_cfg. SysNodes: ~120p",[?MLogPrefix,SysNodes]),
    SysStruct = get_syscfg_part(<<"structure">>,map,SysCfg),
    ?LOG('$trace',"~p. merge_nodes_in_cfg. SysStruct: ~120p",[?MLogPrefix,SysStruct]),
    {CfgX1,TotalNodeNamesMap} = do_merge_sysnodes_in_struct(SysStruct,Cfg),
    CfgX2 = do_merge_sysnodes_in_nodes(SysNodes,CfgX1,TotalNodeNamesMap),
    CfgX2.

%% @private
do_merge_sysnodes_in_nodes(SysNodes,Cfg,TotalNodeNamesMap) ->
    FUpdName = fun(SNode,Acc) ->
                       OldName = maps:get(<<"name">>,SNode),
                       NewNamesList = maps:get(OldName,TotalNodeNamesMap),
                       SNodes = [SNode#{<<"name">> => NewNameBySrv} || {_ServerName,NewNameBySrv} <- NewNamesList],
                       Acc ++ SNodes
               end,
    SysNodes1 = lists:foldl(FUpdName,[],SysNodes),
    FUpd = fun(Val) -> Val++SysNodes1 end,
    maps:update_with(<<"nodes">>,FUpd,[],Cfg).

%% @private
do_merge_sysnodes_in_struct(SysStruct,Cfg) ->
    Struct = maps:get(<<"structure">>,Cfg),
    SysStructNodesRaw = maps:get(<<"nodes">>,SysStruct,[]),
    FMerge = fun(StructItem,{Acc,NodeNamesAcc}) ->
                     ServerName = maps:get(<<"server">>,StructItem,<<"undef">>),
                     {SysStructNodes,NodeNamesMap} = prepare_sysstruct_nodes(SysStructNodesRaw,ServerName),
                     %
                     FUpd = fun(Val) -> Val++SysStructNodes end,
                     Acc1 = [maps:update_with(<<"nodes">>,FUpd,[],StructItem)|Acc],
                     %
                     FMerge = fun(_SysNodeName, SrvToNameMap1, SrvToNameMap2) -> SrvToNameMap1++SrvToNameMap2 end,
                     NodeNamesAcc1 = maps:merge_with(FMerge,NodeNamesAcc,NodeNamesMap),
                     %
                     {Acc1,NodeNamesAcc1}
             end,
    {Struct1,TotalNodeNamesMap} = lists:foldl(FMerge,{[],#{}},Struct),
    {Cfg#{<<"structure">> => lists:reverse(Struct1)},TotalNodeNamesMap}.

%% @private
prepare_sysstruct_nodes(SysStructNodes,ServerName) ->
    FMap = fun(SysStructNode,{Acc,NodeNamesMap}) when is_map(SysStructNode) ->
                   case maps:get(<<"name">>,SysStructNode,undef) of
                       NodeName when is_binary(NodeName) ->
                           NewSysNodeName = <<NodeName/binary,"_",ServerName/binary>>,
                           %
                           XNodeNames = maps:get(NodeName,NodeNamesMap,[]),
                           NodeNamesMap1 = NodeNamesMap#{NodeName => [{ServerName,NewSysNodeName}|XNodeNames]},
                           Acc1 = [SysStructNode#{<<"name">> => NewSysNodeName}|Acc],
                           %
                           {Acc1,NodeNamesMap1};
                       _ -> {Acc++SysStructNode,NodeNamesMap}
                   end
           end,
    lists:foldl(FMap,{[],#{}},SysStructNodes).

%% ------------------
%% Add microservices from syscfg
%% ------------------

merge_microservices_in_cfg(Cfg,SysCfg) ->
    SysMsvcs = get_syscfg_part(<<"microservices">>,list,SysCfg),
    ?LOG('$trace',"~p. merge_microservices_in_cfg. SysNodes: ~120p",[?MLogPrefix,SysMsvcs]),
    %
    FUpd = fun(Val) -> Val++SysMsvcs end,
    maps:update_with(<<"microservices">>,FUpd,[],Cfg).

%% ------------------
%% Add microservices_everywhere from syscfg
%% ------------------

merge_microservices_evw_in_cfg(Cfg,SysCfg) ->
    SysMsvcsEvw = get_syscfg_part(<<"microservices_everywhere">>,list,SysCfg),
    ?LOG('$trace',"~p. merge_microservices_evw_in_cfg. SysMsvcs: ~120p",[?MLogPrefix,SysMsvcsEvw]),
    CfgX1 = do_merge_sysmsvcs_evw_in_msvcs(SysMsvcsEvw,Cfg),
    CfgX2 = do_merge_sysmvcs_evw_in_struct(SysMsvcsEvw,CfgX1),
    CfgX2.

%% @private
do_merge_sysmsvcs_evw_in_msvcs(SysMsvcs,Cfg) ->
    SysMsvcsNames = [maps:get(<<"name">>,SMsvc,undef) || SMsvc <- SysMsvcs],
    FUpd = fun(Val) ->
                   FilteredVal = [V || V <- Val, lists:member(maps:get(<<"name">>,V,undef0),SysMsvcsNames)==false],
                   FilteredVal++SysMsvcs
           end,
    maps:update_with(<<"microservices">>,FUpd,[],Cfg).

%% @private
do_merge_sysmvcs_evw_in_struct(SysMsvcs,Cfg) ->
    SysMsvcsNames = [maps:get(<<"name">>,SMsvc,undef) || SMsvc <- SysMsvcs,maps:get(<<"name">>,SMsvc,undef)/=undef],
    SysMsvcsTypes = [maps:get(<<"type">>,SMsvc,undef) || SMsvc <- SysMsvcs,maps:get(<<"type">>,SMsvc,undef)/=undef],
    Struct = maps:get(<<"structure">>,Cfg),
    Struct1 = do_merge_sysmvcs_evw_in_struct_1(Struct,SysMsvcsNames,SysMsvcsTypes,Cfg,[]),
    Cfg#{<<"structure">> => Struct1}.

%% @private
do_merge_sysmvcs_evw_in_struct_1([],_,_,_,Res) -> Res;
do_merge_sysmvcs_evw_in_struct_1([StructItem|T],SysMsvcsNames,SysMsvcsTypes,Cfg,Acc) ->
    StructNodes = maps:get(<<"nodes">>,StructItem),
    FAddSysMsvc = fun(NodeItem) ->
                          StructMsvcs = maps:get(<<"microservices">>,NodeItem,[]),
                          StructMsvcsFiltered = do_filter_structmsvcs_evw_by_sysmsvcs(StructMsvcs,SysMsvcsTypes,Cfg),
                          NewStructMsvcs = SysMsvcsNames++StructMsvcsFiltered,
                          NodeItem#{<<"microservices">> => NewStructMsvcs}
                  end,
    StructNodes1 = lists:map(FAddSysMsvc,StructNodes),
    Acc1 = [StructItem#{<<"nodes">> => StructNodes1}|Acc],
    do_merge_sysmvcs_evw_in_struct_1(T,SysMsvcsNames,SysMsvcsTypes,Cfg,Acc1).

%% @private
do_filter_structmsvcs_evw_by_sysmsvcs(StructMsvcs,SysMsvcsTypes,Cfg) ->
    Msvcs = maps:get(<<"microservices">>,Cfg,[]),
    FilteredMsvcs = [Msvc || Msvc <- Msvcs,
                             lists:member(maps:get(<<"name">>,Msvc,undef),StructMsvcs)==true,
                             lists:member(maps:get(<<"type">>,Msvc,undef),SysMsvcsTypes)==false],
    [maps:get(<<"name">>,Msvc,undef) || Msvc <- FilteredMsvcs].

%% ------------------
%% Support functions
%% ------------------

get_syscfg_part(PartName,list,SysCfg) ->
    case maps:get(PartName,SysCfg,undef) of
        undef ->
            ?LOG('$trace',"~p. get_syscfg_part. for list. '~s' key is missed in config",[?MLogPrefix,PartName]),
            [];
        SysCfgItems when is_list(SysCfgItems) ->
            FIsMap = fun(M) -> erlang:is_map(M) end,
            {ValidItems,InvalidItems} = lists:partition(FIsMap,SysCfgItems),
            case erlang:length(InvalidItems)/=0 of
                true ->
                    ?LOG('$trace',"~p. get_syscfg_part. for list. invalid '~s'. items must be object. Selected items are skipped~n~120p",[?MLogPrefix,PartName,InvalidItems]);
                false -> ok
            end,
            ValidItems;
        _ ->
            ?LOG('$trace',"~p. get_syscfg_part. for list. the value '~s' is not a list of objects",[?MLogPrefix,PartName]),
            []
    end;
get_syscfg_part(PartName,map,SysCfg) ->
    case maps:get(PartName,SysCfg,undef) of
        undef ->
            ?LOG('$trace',"~p. get_syscfg_part. for map. '~s' key is missed in config",[?MLogPrefix,PartName]),
            #{};
        SysCfgItem when is_map(SysCfgItem) -> SysCfgItem;
        _ ->
            ?LOG('$trace',"~p. get_syscfg_part. for map. the value '~s' is not a list of objects",[?MLogPrefix,PartName]),
            #{}
    end.