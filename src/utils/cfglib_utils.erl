%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 02.01.2021
%%% @doc

-module(cfglib_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([drop_last_subdir/2,
         ensure_dir/1,
         read_json_file/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

-define(MLogPrefix, "Utils").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec ensure_dir(DirPath::binary()) -> ok | {error,Reason::file:posix()}.
%% ---
ensure_dir(Path) when is_binary(Path) ->
    PathToCreate = <<Path/binary,"/">>,
    filelib:ensure_dir(PathToCreate).

%% ---
-spec drop_last_subdir(Path::string(),N::integer()) -> NPath::string().
%% ---
drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ---
-spec read_json_file(FilePath::binary()) -> {ok,Data::map()} | {error,ErrMap::map()}.
%% ---
read_json_file(FilePath) ->
    ?LOG('$trace',"~p. read_json_file. FilePath:~120p",[?MLogPrefix,FilePath]),
    case file:read_file(FilePath) of
        {ok,DataRaw} ->
            case catch jsx:decode(DataRaw,[return_maps]) of
                {'EXIT',_} -> {error,<<"invalid json. decode (",FilePath/binary,") error">>};
                CfgMap -> {ok,CfgMap}
            end;
        {error,Err} ->
            ?LOG('$trace',"~p. read_json_file. read file error. FilePath:~120p~nErr:~120p",[?MLogPrefix,FilePath,Err]),
            {error,<<"read error. file (",FilePath/binary,")">>}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================