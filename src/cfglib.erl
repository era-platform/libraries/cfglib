%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 06.01.2021 17:53
%%% @doc

-module(cfglib).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([start/0, start/2,
         stop/0, stop/1,
         %
         load_cfg/1, load_cfg/2,
         validate_cfg/2,
         validate_cfg_raw/3,
         get_curr_cfg/0,
         get_curr_cfg_raw/0,
         get_prev_cfg/0,
         get_msvc_descriptor_info/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("cfglib.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------
%% Application
%% ------------------

%% ---
-spec start() -> ok | {error,Reason::term()}.
%% ---
start() ->
    setup_deps_env(),
    Result = application:ensure_all_started(?APP, permanent),
    ?OUTL("Application start. Result:~120p",[Result]),
    case Result of
        {ok, _Started} -> ok;
        {error,_}=Error -> Error
    end.

%% ---
-spec stop() -> ok | {error,Reason::term()}.
%% ---
stop() ->
    application:stop(?APP).

%% ---
start(_Mode, State) ->
    ?OUTL("Application start(Mode, State)", []),
    ?SUPV:start_link(State).

%% ---
stop(_State) ->
    ok.

%% ------------------
%% Cfg
%% ------------------

%% ---
-spec load_cfg(Cfg::map(), SysCfg::map()) -> {ok,ResMap::map()} | {error,ErrMap::map()}.
%% ---
load_cfg(Cfg,SysCfg) when is_map(Cfg) andalso is_map(SysCfg) ->
    Cfg1 = ?CDEF:apply_defaults(Cfg),
    {ok,CfgX} = ?CSYS:apply_syscfg(Cfg1,SysCfg),
    do_load_cfg(Cfg,CfgX).

%% ---
-spec load_cfg(Cfg::map()) -> {ok,ResMap::map()} | {error,ErrMap::map()}.
%% ---
load_cfg(Cfg0) when is_map(Cfg0) ->
    Cfg = ?CDEF:apply_defaults(Cfg0),
    do_load_cfg(Cfg0,Cfg).

%% @private
do_load_cfg(CfgRaw,Cfg) ->
    PrevCfg = ?CU:get_prev_cfg(),
    ?DMsvc:load_app_descriptors(), % @todo переделать загрузку дескрипторов приложений на подгрузку в случае отсутствия информации в етс дескрипторов перед валидацией или устанавливать флаг в апп сторадж и при наличии не лоадить дескрипторы
    case validate_cfg(Cfg,PrevCfg) of
        {ok,_ResMap}=Ok ->
            case catch ?CU:create_curr_cfg_beam(CfgRaw,Cfg) of
                {'EXIT',ErrDescr0} -> {error,make_error(create_cfg_beam_crash, ErrDescr0)};
                ok -> Ok;
                {error,ErrDescr1} -> {error,make_error(create_cfg_beam_error, ErrDescr1)}
            end;
        {error,_}=Err1 -> Err1
    end.

%% ---
-spec validate_cfg(Cfg::map(), OldCfg::map() | undefined) -> {ok, ResMap::map()} | {error,ErrMap::map()}.
%% ---
validate_cfg(Cfg,OldCfg) when is_map(Cfg) ->
    ?VlCfg:validate_cfg(Cfg,OldCfg).

%% ---
-spec validate_cfg_raw(CfgRaw::map(), OldCfg::map() | undefined, SysCfg::map()) -> {ok,ResMap::map()} | {error,ErrMap::map()}.
%% ---
validate_cfg_raw(CfgRaw,OldCfg,SysCfg) when is_map(CfgRaw) ->
    Cfg1 = ?CDEF:apply_defaults(CfgRaw),
    {ok,CfgX} = ?CSYS:apply_syscfg(Cfg1,SysCfg),
    ?VlCfg:validate_cfg(CfgX,OldCfg).

%% ---
-spec get_curr_cfg() -> Cfg::map() | no_return().
%% ---
get_curr_cfg() -> ?CU:get_curr_cfg().

%% ---
-spec get_curr_cfg_raw() -> CfgRaw::map() | no_return().
%% ---
get_curr_cfg_raw() -> ?CU:get_curr_cfg_raw().

%% ---
-spec get_prev_cfg() -> Cfg::map() | undefined.
%% ---
get_prev_cfg() -> ?CU:get_prev_cfg().

%% ---
-spec get_msvc_descriptor_info(MsvcName::binary(),Key::[atom()] | atom()) -> Result::term() | no_return.
%% ---
get_msvc_descriptor_info(MsvcName,Keys) when is_list(Keys) ->
    [get_msvc_descriptor_info(MsvcName,Key) || Key <- Keys];
get_msvc_descriptor_info(MsvcName,Key) when is_atom(Key) ->
    ?DMsvc:get_descriptor_info(MsvcName,Key).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
make_error(ErrText, ErrReason) ->
    #{error => ErrText, reason => ErrReason}.

%% ---
setup_deps_env() ->
    ?BU:set_env(?APPBL,max_loglevel,'$info').