= cfglib
:toc-title: Содержание
:hardbreaks:
:toclevels: 1
:toc:


== Описание

.Ответственности
* производит валидацию конфигурации
* производит загрузку конфигурации
* выполняет загрузку дескрипторов ролей
* предоставляет функции для работы с конфигурацией

=== Зависимости

* basiclib
* loggerlib
* jsx

== Сборка

* Используемая версия erlang: 23.
* Используется rebar3

Для билда необходимо выполнить `make`.

Например:
[source,bash]
----
$ cd /home/user/develop/era/cfglib
$ make
----

== Использование при разработке приложений

Во время работы системы на каждой erlang ноде первым будет запускаться приложение cfgstarter. Именно он подготавливает ноду для запуска целевых приложений, в частности - загружает конфигурацию.

Поэтому для работы во время разработки достаточно добавить пути до кода и выполнить загрузку конфигурации через вызов функций модуля `cfg_dev`.

Например:
[source,erlang]
----
1>code:add_path("/home/user/develop/era/cfglib/_build/default/lib/cfglib/ebin").
2>cfglib_dev:load_cfg(<<"/home/user/develop/configs/cfg.json">>).
----

== Интерфейсы

=== Модуль cfglib_dev

Предоставляет функции для разработчика приложений.

==== Exports

.load_cfg/1
[source,erlang]
----
-spec load_cfg(CfgPath::binary()) -> {ok,loaded} | {error,Reason::term()}.
----

* Считывает конфигурацию с диска.
* Выполняет декодинг в формат json.
* Генерирует *.beam файл содержащий конфигурацию (если предудущие пункты были выполнены успешно).

Например:
[source,erlang]
----
1>cfglib_dev:load_cfg(<<"/home/user/develop/configs/cfg.json">>).
----

.validate/1
[source,erlang]
----
-spec validate(Cfg::binary()) -> {ok,ResMap::map()} | {error,ErrMap::map()}.
----

* Считывает конфигурацию с диска.
* Выполняет декодинг в формат json.
* Производит валидацию конфигурации.

Например:
[source,erlang]
----
1>cfglib_dev:validate(<<"/home/user/develop/configs/cfg.json">>).
----