%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 06.01.2021 17:54
%%% @doc

-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%% ====================================================================
%% Define modules and variables
%% ====================================================================

%% Application
-define(APP, cfglib).
-define(ServiceName, "CfgLib"). % prefix for output in logs
-define(SUPV, cfglib_supv).

%% App env
-define(EnvSrvNode, server_node).

%% other apps
-define(APPBL, basiclib).
-define(APPJSV, jsv).

%% Descriptors
-define(DMsvc, cfglib_descriptor_msvc).
-define(DMvscVl, cfglib_descriptor_msvc_validate).
-define(DescrEts, cfglib_descriptor_ets).

%% Validator
-define(VlCfg, cfglib_validator).
-define(VlCfgCheckStruct, cfglib_validator_checks_structure).
-define(VlCfgCheckMsvcs, cfglib_validator_checks_msvcs).

%% Cfg
-define(CU, cfglib_cfg_utils).
-define(CPB, cfglib_cfg_parser_base).
-define(CPS, cfglib_cfg_parser_special).
-define(CSYS, cfglib_cfg_syscfg).
-define(CDEF, cfglib_cfg_defaults).

%% Common
-define(U, cfglib_utils).

%% basiclib app
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).

%% ====================================================================
%% Define Records
%% ====================================================================

%% descriptor.msvc
-record(msvc_descriptor, {
                          name ::binary(),
                          start_point=[] ::list(),
                          validate_params_point=[] :: undefined | list(),
                          update_params_point=[] :: undefined | list(),
                          update_cfg_point=[] :: undefined | list(),
                          params ::map(),
                          app_path ::binary(),
                          os_cmd_params= <<>> ::binary(),
                          erl_cmd_params= <<>> ::binary()
                         }).

%% validator
-record(check_state, {
                      ncfg :: undefined | map(), % new cfg
                      ocfg :: undefined | map(), % old cfg
                      warnings=[] :: list(),
                      errors=[] :: list()
                     }).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {cfglib,erl}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?LOGFILE,{Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?LOGFILE,Text)).

-define(OUT(Text), ?BLlog:out(Text)).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level,?LOGFILE,{Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level,?LOGFILE,Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).

%% local
-define(OUTL(Fmt,Args), io:format("~s. "++Fmt++"~n",[?ServiceName]++Args)).
-define(OUTL(Text), io:format("~s. "++Text++"~n",[?ServiceName])).